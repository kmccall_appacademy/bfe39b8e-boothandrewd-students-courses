class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return if @courses.include?(course)
    raise Exception if has_conflict?(course)
    @courses.push(course)
    course.students.push(self)
  end

  def course_load
    @courses.each_with_object(Hash.new(0)) do |course, loads|
      loads[course.department] += course.credits
    end
  end

  def has_conflict?(course)
    @courses.any? do |enrolled_course|
      enrolled_course.conflicts_with?(course)
    end
  end
end
